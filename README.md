# whatfiles

A build for [spieglt/whatfiles](https://github.com/spieglt/whatfiles).

> Log what files are accessed by any Linux process

## Download

The binary is kept as an artifact for an easy download.

* [Binary](https://gitlab.com/l3tompouce/builders/whatfiles/-/jobs/artifacts/master/raw/whatfiles?job=build)
* [Version information](https://gitlab.com/l3tompouce/builders/whatfiles/-/jobs/artifacts/master/raw/whatfiles.version?job=build)

## Upstream

This pipeline was discuted in [spieglt/whatfiles#3](https://github.com/spieglt/whatfiles/issues/3).

Version `1.0.0` was released since then, with binaries for
[x64](https://github.com/spieglt/whatfiles/releases/download/v1.0/whatfiles_x64),
[x86](https://github.com/spieglt/whatfiles/releases/download/v1.0/whatfiles_x86),
[arm64](https://github.com/spieglt/whatfiles/releases/download/v1.0/whatfiles_arm64)
and [arm32](https://github.com/spieglt/whatfiles/releases/download/v1.0/whatfiles_arm32)
architectures.
